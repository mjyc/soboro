# Examples

- ["Brown Bear, Brown Bear, What Do You See?" Interactive Storytelling](./brbear.sbr) <!-- [[spec](./brbear.json)] -->
- [Neck Exercise Coach](./neckex.sbr) <!-- [[spec](./neckex.json)] -->
- [Recipe instructions](./recipe.sbr) <!-- [[spec](./recipe.json)] -->

<!-- - [Backchannel opportunity detector] -->
<!-- - [Hand gesture detector] -->
