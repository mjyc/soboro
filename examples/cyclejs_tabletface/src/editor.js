const xs = require("xstream").default;
const sampleCombine = require("xstream/extra/sampleCombine").default;
const { makeDOMDriver } = require("@cycle/dom");
const { run } = require("@cycle/run");
const { makeFirestoreDriver } = require("soboro");
const Editor = require("soboro/src/frontend/Editor.js");

let firebaseConfig;
try {
  firebaseConfig = require("./firebaseConfig.json");
} catch (err) {
  console.error("./firebaseConfig.json does not exist");
}

const main = (sources) => {
  const editor = Editor(sources);

  const text$ = editor.events.filter((x) => x.type === "text");
  const run$ = editor.events.filter((x) => x.type === "run");
  const stop$ = editor.events.filter((x) => x.type === "stop");
  // TODO: use "category"
  const firestore$ = xs.merge(
    run$
      .compose(sampleCombine(text$))
      .filter((x) => {
        try {
          JSON.parse(x[1].value);
          return true;
        } catch (err) {
          console.error("Invalid input", x[1]);
          return false;
        }
      })
      .map((x) => {
        const stamp = Date.now();
        const raw = x[1].value;
        const ast = JSON.parse(raw);
        const prog = {
          stamp,
          raw,
          ast,
        };
        return [
          {
            method: "collection",
            args: ["programs"],
          },
          {
            method: "add",
            args: [prog],
          },
        ];
      }),
    stop$.map(() => {
      const stamp = Date.now();
      return [
        {
          method: "collection",
          args: ["programs"],
        },
        {
          method: "doc",
          args: ["cancel"],
        },
        {
          method: "set",
          args: [
            {
              stamp,
            },
          ],
        },
      ];
    })
  );
  return Object.assign(
    {
      Firestore: firestore$,
    },
    editor
  );
};

const drivers = {
  DOM: makeDOMDriver("#app"),
  Firestore: makeFirestoreDriver(firebaseConfig),
};

run(main, drivers);
