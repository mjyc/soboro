const xs = require("xstream").default;
const sampleCombine = require("xstream/extra/sampleCombine").default;
const { run } = require("@cycle/run");
const { div, makeDOMDriver } = require("@cycle/dom");
const { makeFirestoreDriver } = require("soboro");
const {
  ProgramSourceNames,
  ProgramSinkNames,
} = require("./withTabletFaceRobot");

const { timeDriver } = require("@cycle/time");
const TimeTravel = require("@mjyc/cycle-time-travel").default;

let firebaseConfig;
try {
  firebaseConfig = require("./firebaseConfig.json");
} catch (err) {
  console.error("./firebaseConfig.json does not exist");
}

// TODO: note that this viz does not care about programId
const main = (sources) => {
  // TODO: update to use "category"
  const programData$ = sources.Firestore.flatten()
    .filter((x) => {
      if (!x.docChanges) return false;
      const docs = x.docChanges().filter((dc) => dc.type === "added");
      return docs.length === 1;
    })
    .map((x) => {
      return x.docChanges()[0].doc.data();
    });

  const remoteSources = ProgramSourceNames.reduce((prev, name) => {
    prev[name] = programData$
      .filter((x) => {
        return x.type === "source" && x.name === name;
      })
      .map((x) => {
        return x.value;
      });
    return prev;
  }, {});
  const remoteSinks = ProgramSinkNames.reduce((prev, name) => {
    prev[name] = programData$
      .filter((x) => {
        return x.type === "sink" && x.name === name;
      })
      .map((x) => {
        return x.value;
      });
    return prev;
  }, {});

  // TODO: "play" it on start
  const timeTravel = TimeTravel(
    sources.DOM,
    sources.Time,
    Object.keys(remoteSources)
      .map((name) => ({ stream: remoteSources[name], label: name }))
      .concat(
        Object.keys(remoteSinks).map((name) => ({
          stream: remoteSinks[name],
          label: name,
        }))
      )
  );

  const vdom$ = timeTravel.DOM;
  // TODO: update to use "category"
  const firebase$ = xs.of([
    { method: "collection", args: ["programData"] },
    { method: "onSnapshot", args: [] },
  ]);
  return {
    DOM: vdom$,
    Firestore: firebase$,
  };
};

const drivers = {
  DOM: makeDOMDriver("#app"),
  Time: timeDriver,
  Firestore: makeFirestoreDriver(firebaseConfig),
};

run(main, drivers);
