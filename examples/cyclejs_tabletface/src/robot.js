const xs = require("xstream").default;
const delay = require("xstream/extra/delay").default;
const sampleCombine = require("xstream/extra/sampleCombine").default;
const { run } = require("@cycle/run");
const {
  createTabletFaceRobotSandboxDrivers,
} = require("tabletrobotface-starter-lib");
const { makeProgramExecutor, makeFirestoreDriver } = require("soboro");
const withTabletFaceRobot = require("./withTabletFaceRobot").default;
const { ProgramSinkNames } = require("./withTabletFaceRobot");

let firebaseConfig;
try {
  firebaseConfig = require("./firebaseConfig.json");
} catch (err) {
  console.error("./firebaseConfig.json does not exist");
}

const main = (sources) => {
  // TODO: use "category"
  const programData$ = sources.Firestore.filter(
    (x) => x.constructor.name === "Stream"
  )
    .flatten()
    .drop(1) // first snapshot contains all docs in the collection
    .filter((x) => {
      if (!x.docChanges) return false;
      const docs = x
        .docChanges()
        .filter((doc) => doc.type === "added" || doc.doc.id === "cancel");
      return docs.length >= 1;
    })
    .map((x) => {
      const docs = x
        .docChanges()
        .filter((doc) => doc.type === "added" || doc.doc.id === "cancel");
      if (docs.length > 1) console.warn("more than 1 program added");
      const program = Object.assign({ id: docs[0].doc.id }, docs[0].doc.data());
      return program;
    });
  const program$ = programData$.map((program) => {
    return program.id === "cancel" ? null : program.ast;
  });
  const started$ = program$.compose(delay(500)).mapTo(true);
  started$.addListener({ next: () => {} }); // force start

  const ProgramExecutor = makeProgramExecutor({
    ProgramSinkNames,
  });
  const robotSources = Object.assign({}, sources, {
    program: program$,
    started: started$,
  });
  const robotSinks = withTabletFaceRobot(ProgramExecutor)(robotSources);

  // TODO: use "category"
  const firebase$ = xs.merge(
    xs.of([
      {
        method: "collection",
        args: ["programs"],
      },
      {
        method: "onSnapshot",
        args: [],
      },
    ]),
    robotSinks.data.compose(sampleCombine(programData$)).map(([d, pd]) => {
      const data = Object.assign({ programId: pd.id }, d);
      return [
        {
          method: "collection",
          args: [`programData`],
        },
        {
          method: "add",
          args: [data],
        },
      ];
    })
  );
  const sinks = Object.assign(
    {
      Firestore: firebase$,
    },
    robotSinks
  );
  return sinks;
};

const drivers = Object.assign(
  { Firestore: makeFirestoreDriver(firebaseConfig) },
  createTabletFaceRobotSandboxDrivers()
);

run(main, drivers);
