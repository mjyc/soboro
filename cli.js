const fs = require("fs");
const { parser } = require(".");

console.log(
  JSON.stringify(parser.parse(fs.readFileSync(process.argv[2]).toString()))
);
