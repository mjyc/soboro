const fs = require("fs");
const { mockTimeSource } = require("@cycle/time");
const {
  interpret,
  makeProgramExecutor,
  fsSyncDriver,
  makeFirestoreDriver,
} = require("./../");

describe("interpret", () => {
  test("0", (done) => {
    const prog = [
      { trigger: "started", actions: [{ name: "say", value: "hello" }] },
    ];

    const Time = mockTimeSource();
    const data = {
      h: "hello",
    };
    const sources = {
      started: Time.diagram("-x---|"),
    };
    const expectedSinks = {
      say: Time.diagram("-h---|", data),
    };

    const actualSinks = interpret(prog, sources);
    Time.assertEqual(actualSinks.say, expectedSinks.say);
    Time.run(done);
  });

  test("1", (done) => {
    const prog = [
      {
        trigger: { op: "or", value: ["started", "sayDone"] },
        actions: [{ name: "displayButtons", value: ["blue", "red"] }],
      },
      {
        trigger: { op: "equal", value: ["buttonClick", "blue"] },
        actions: [{ name: "say", value: "blue" }],
      },
      {
        trigger: { op: "equal", value: ["buttonClick", "red"] },
        actions: [{ name: "say", value: "red" }],
      },
    ];

    const Time = mockTimeSource();
    const lookup = {
      t: true,
      c: ["blue", "red"],
      b: "blue",
      r: "red",
    };
    const sources = {
      started: Time.diagram("t------|", lookup),
      buttonClick: Time.diagram("-b--r--|", lookup),
      sayDone: Time.diagram("---t--t|", lookup),
    };
    const expectedSinks = {
      displayButtons: Time.diagram("c--c--c|", lookup),
      say: Time.diagram("-b--r--|", lookup),
    };

    const actualSinks = interpret(prog, sources);
    Time.assertEqual(actualSinks.displayButtons, expectedSinks.displayButtons);
    Time.assertEqual(actualSinks.say, expectedSinks.say);
    Time.run(done);
  });
});

describe("makeProgramExecutor", () => {
  test("0", (done) => {
    const Time = mockTimeSource();

    const prog = [
      { trigger: "started", actions: [{ name: "say", value: "hello" }] },
    ];
    const lookup = {
      t: true,
      h: "hello",
      p: prog,
    };
    const sources = {
      program: Time.diagram("-p---|", lookup),
      started: Time.diagram("---t-|", lookup),
    };
    const expectedSinks = {
      say: Time.diagram("---h-|", lookup),
    };

    const progExer = makeProgramExecutor({
      ProgramSinkNames: ["say"],
    });
    const actualSinks = progExer(sources);

    Time.assertEqual(actualSinks.say, expectedSinks.say);
    Time.run(done);
  });

  test("1", (done) => {
    const Time = mockTimeSource();

    const prog = [
      {
        trigger: { op: "or", value: ["started", "sayDone"] },
        actions: [{ name: "displayButtons", value: ["blue", "red"] }],
      },
      {
        trigger: { op: "equal", value: ["buttonClick", "blue"] },
        actions: [{ name: "say", value: "blue" }],
      },
      {
        trigger: { op: "equal", value: ["buttonClick", "red"] },
        actions: [{ name: "say", value: "red" }],
      },
    ];
    const lookup = {
      t: true,
      c: ["blue", "red"],
      b: "blue",
      r: "red",
      p: prog,
    };
    const sources = {
      program: Time.diagram("p-------|", lookup),
      started: Time.diagram("-t------|", lookup),
      buttonClick: Time.diagram("--b--r--|", lookup),
      sayDone: Time.diagram("----t--t|", lookup),
    };
    const expectedSinks = {
      displayButtons: Time.diagram("-c--c--c|", lookup),
      say: Time.diagram("--b--r--|", lookup),
    };

    const progExer = makeProgramExecutor({
      ProgramSinkNames: ["say", "displayButtons"],
    });
    const actualSinks = progExer(sources);

    Time.assertEqual(actualSinks.displayButtons, expectedSinks.displayButtons);
    Time.assertEqual(actualSinks.say, expectedSinks.say);
    Time.run(done);
  });
});

describe("fsSyncDriver", () => {
  const testFilename = "./testout";

  afterAll(() => {
    fs.unlinkSync("./testout");
  });

  test("0", (done) => {
    const Time = mockTimeSource();
    const expected = { a: 1 };
    const lookup = {
      a: {
        method: "writeFileSync",
        args: [testFilename, JSON.stringify(expected)],
      },
    };
    const command$ = Time.diagram("-a---|", lookup);
    const fsSyncOut$ = fsSyncDriver(command$);
    fsSyncOut$.addListener({
      next: (x) => {
        const result = JSON.parse(fs.readFileSync(testFilename));
        expect(result).toEqual(expected);
        done();
      },
    });
    Time.run();
  });
});

// TODO: double check if I need to use "category"
describe("makeFirestoreDriver", () => {
  const firebaseConfigFilename = "./firebaseConfig.json";
  const testData = {
    first: "Ada",
    last: "Lovelace",
    born: 1815,
  };

  test("add", (done) => {
    let config;
    try {
      config = JSON.parse(fs.readFileSync(firebaseConfigFilename));
    } catch (err) {
      console.warn(firebaseConfigFilename, "does not exist");
      done();
      return;
    }

    const Time = mockTimeSource();
    const lookup = {
      a: [
        {
          method: "collection",
          args: ["_test"],
        },
        {
          method: "doc",
          args: ["test"],
        },
        {
          method: "set",
          args: [testData],
        },
      ],
    };
    const command$ = Time.diagram("a|", lookup);
    const firestoreDriver = makeFirestoreDriver(config);
    const firestore$ = firestoreDriver(command$);
    firestore$.addListener({
      next: (x) => {
        x.addListener({
          next: () => {
            done();
          },
        });
      },
    });
    Time.run();
  });

  test("get", (done) => {
    let config;
    try {
      config = JSON.parse(fs.readFileSync(firebaseConfigFilename));
    } catch (err) {
      console.warn(firebaseConfigFilename, "does not exist");
      done();
      return;
    }

    const Time = mockTimeSource();
    const lookup = {
      a: [
        {
          method: "collection",
          args: ["_test"],
        },
        {
          method: "doc",
          args: ["test"],
        },
        {
          method: "get",
          args: [],
        },
      ],
    };
    const command$ = Time.diagram("a|", lookup);
    const firestoreDriver = makeFirestoreDriver(config);
    const firestore$ = firestoreDriver(command$);
    firestore$.addListener({
      next: (x) => {
        x.addListener({
          next: (y) => {
            expect(y.data()).toEqual(testData);
            done();
          },
        });
      },
    });
    Time.run();
  });

  test("delete", (done) => {
    let config;
    try {
      config = JSON.parse(fs.readFileSync(firebaseConfigFilename));
    } catch (err) {
      console.warn(firebaseConfigFilename, "does not exist");
      done();
      return;
    }

    const Time = mockTimeSource();
    const lookup = {
      a: [
        {
          method: "collection",
          args: ["_test"],
        },
        {
          method: "doc",
          args: ["test"],
        },
        {
          method: "delete",
          args: [],
        },
      ],
      b: [
        {
          method: "collection",
          args: ["_test"],
        },
        {
          method: "doc",
          args: ["test"],
        },
        {
          method: "get",
          args: [],
        },
      ],
    };
    const command$ = Time.diagram("ab|", lookup);
    const firestoreDriver = makeFirestoreDriver(config);
    const firestore$ = firestoreDriver(command$);
    firestore$.addListener({
      next: (x) => {
        x.addListener({
          next: (y) => {
            if (typeof y === "undefined") return;
            expect(y.exists).toBe(false);
            done();
          },
        });
      },
    });
    Time.run();
  });

  test("doc.onSnapshot", (done) => {
    let config;
    try {
      config = JSON.parse(fs.readFileSync(firebaseConfigFilename));
    } catch (err) {
      console.warn(firebaseConfigFilename, "does not exist");
      done();
      return;
    }

    const Time = mockTimeSource();
    const lookup = {
      a: [
        {
          method: "collection",
          args: ["_test"],
        },
        {
          method: "doc",
          args: ["test"],
        },
        {
          method: "set",
          args: [testData],
        },
      ],
      b: [
        {
          method: "collection",
          args: ["_test"],
        },
        {
          method: "doc",
          args: ["test"],
        },
        {
          method: "onSnapshot",
          args: [],
        },
      ],
      c: [
        {
          method: "collection",
          args: ["_test"],
        },
        {
          method: "doc",
          args: ["test"],
        },
        {
          method: "set",
          args: [Object.assign({}, testData, { female: true })],
        },
      ],
      d: [
        {
          method: "collection",
          args: ["_test"],
        },
        {
          method: "doc",
          args: ["test"],
        },
        {
          method: "set",
          args: [{}],
        },
      ],
      e: [
        {
          method: "collection",
          args: ["_test"],
        },
        {
          method: "doc",
          args: ["test"],
        },
        {
          method: "delete",
          args: [],
        },
      ],
    };
    const command$ = Time.diagram("abcd|", lookup);
    const firestoreDriver = makeFirestoreDriver(config);
    const firestore$ = firestoreDriver(command$);
    const expecteds = [
      testData,
      Object.assign({}, testData, { female: true }),
      {},
    ];
    let i = 0;
    firestore$.addListener({
      next: (x) => {
        x.addListener({
          next: (y) => {
            i++;
            if (i === 6) done();
            if (typeof y === "undefined") return;
            const actual = y.data();
            const expected = expecteds.shift();
            expect(expected).toEqual(actual);
          },
        });
      },
    });
    Time.run();
  });
});
