from rx import operators as ops


def interpret(prog, sources):
    print("sources", sources)
    sinks = {}
    for rule in prog:
        for action in rule["actions"]:
            sinks[action["name"]] = sources[rule["trigger"]].pipe(
                ops.map(lambda i: action["value"])
            )
    print("sinks", sinks)
    return sinks
