import unittest

from rx import operators as ops
from rx.testing.marbles import marbles_testing
from soboro import interpret


class TestInterpret(unittest.TestCase):
    def test_0(self):
        with marbles_testing() as (start, cold, hot, exp):
            data = {"t": True, "h": "hello"}
            expected = exp("-h----|", data)

            prog = [
                {
                    "trigger": "started",
                    "actions": [{"name": "say", "value": "hello"}],
                },
            ]

            started = hot("-t----|", data)
            sources = {"started": started}
            sinks = interpret(prog, sources)

            actual = start(sinks["say"])
            assert actual == expected


if __name__ == "__main__":
    unittest.main()
