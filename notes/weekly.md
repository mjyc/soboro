# Weekly

## Week 29

Goals

- [ ] create one example and explain about it in the doc e.g., markdox
  - [ ] e.g., interactive storytelling without the database integration
  - [ ] create an environment/human simulator and test the program using fs-check and ltljs/ts with the FSM2arbitrary idea

## Week 27

Done

- thought about AR/VR integration
  - https://ar-js-org.github.io/AR.js-Docs/
  - https://aframe.io/

Goals

- [x] re-organize this repo to reflect evaluation idea discussed in Week 26
  - [ ] create evaluations folder, write down tasks, write programs in each languages & models

## Week 26

Done

- thought about [contributions](./contributions.md) and [disgested this week's discussion with Maya](./ideas.md#awesome-robotics-project)

## Week 25

Goals

1. [ ] come up with a hypothesis for testing soboro with the EMAR team, e.g., will they use v1? why / why not?
2. [ ] nail down the DSL based on the defined project purpose; at least define the AST
3. [ ] if 1. & 2. fails, just prepare examples myself, e.g., the three go-to social robot tasks, one hierarchical task, one multi-robot task
4. (optional) cozmo integration or look for a richer platform

## Week 23

Goals

- [ ] come up with hypothesis for testing soboro with the EMAR team, e.g., will they use v1? why / why not?
- MVP
  - [x] infra: DB integration for connecting all components
  - [x] component1: editor and program-controller UI
  - [x] component2: interpreter and program-controller
  - [x] component3: robot monitor

## Week 20

Goals

- cont. design
  - pause & resume
  - constraint
  - Dooley's flowchart (fine-grained timing control)
  - Continuous outputs
  - FSM, flowchart like program visualization for debugging
  - roscli-like or offline simulation-based debugging tool
- working prototype
- DBT tasks
  - e.g., mindful, emotion regulation, distress tolerance, interpersonal effectiveness
  - collaboration

## Week 19

Done

- started designing a DSL
  - related opensource projects
    - https://github.com/ChatScript/ChatScript/blob/master/WIKI/ChatScript-Basic-User-Manual.md#overview
    - https://mermaid-js.github.io/mermaid/#/flowchart
  - related academic work
    - Vega-lite: A grammar of interactive graphics by Satyanarayan et al
    - Exploring relationships between interaction attributes and experience by Lenz et al
    - Design Patterns for Exploring and Prototyping Human-Robot Interactions by Sauppe et al
    - Recognizing Engagement in Human-Robot Interaction by Rich et al
