## Overview

- Social robots should have as many skills as voice agents like Amazon Alexa, however, creating content for social robots is hard.
- We propose a markup language for describing social robot behaviors to make behavior authoring easier.
- Alternatively, we could provide a direct programming interface.

## Contributions

- Presents an expressive and concise DSL to specify interactive robot behaviors
- Presents a rigorous definition for the DSL, which allows it to be portable

## Related Work

- EUP for social robots
- Robot Behavior Presentations

## DSL

...

## Examples

_highlight the expressiveness and conciseness_

## Case Studies

- ACT/DBT
- Creating cozmo, temi applications with U. of Wisc. collaborators
