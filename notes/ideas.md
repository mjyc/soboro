# Ideas

## 2020-08-24

- features
  - document the state and event composition grammar
  - create a temporal grammar/trigger/rule/operator
  - document action interruption handling
  - create an action grammar
  - demo complex events like BOB & hammer.js
  - demo complex actions idea1
    - state => action
    - event => controller
      <!-- - single => multiple -->
      <!-- - multiple => single -->
  - demo complex actions idea2
    - multiple channels

## 2020-08-22

- intro
  - extensibility
  - cross platform/language
- documentation inspirations
  - https://vega.github.io/vega-lite/
  - https://github.com/errata-ai/vale
  - https://github.com/staltz/xstream
  - https://github.com/superscriptjs/superscript/wiki
  - https://mermaid-js.github.io/mermaid/#/
  - https://ar-js-org.github.io/AR.js-Docs/

## 2020-07-21

- documentation
  - syntax
    - (for {n/every} times) when {event} {action} (then {action} ...)
    - (for {n/every} times) while {state} {action_pattern}
  - main contents
    - matching "rules" by order
    - the difference btw "when" & "while"
      - there are "events" and "states"; we'll explain them shortly
    - "for every times when/while" -> "whenever"/"whilever"
    - "action"
      - there are two types; we'll explain them shortly
    - "then" for chaining
    - events & states
      - exogenous
      - robot action
    - instantaneous & durative actions
    - (automatic) type conversion
      - "become" for the state->event conversion
      - "last" for the event->state conversion
      - follow_face -> start/stop/toggle follow_face
- notes
  - possible naming style
    - "humanFace" (js)
    - "HumanFace" (js)
    - "human_face" (python)
    - "HUMAN_FACE" (spreadsheet)
  - property-based testing (via fast-check)
  - explore robot + IoT + AR/VR use cases, e.g.,
    - a multi-robot interaction demo
    - ar robot control interface
  - write the method part in overleaf based on "doc"

---

## 2020-07-08

More evaluation ideas:

- use npm packages, codesandbox, simulator code for demo & testing
  - first tabletface and then integrate with cozmo
- github actions, jest for testing
- when ready, publish this project as a github/lab repo

---

## 2020-06-23

### Suggestions from Maya

- Find show cases to highlight the power of DSL, e.g., compare with
  - ros, \psi (they are essentially pub/sub)
  - pub/sub like meteor/firebase
  - models like FSM, BT, Petri
- User or case study with internal users, e.g., EMAR team memebers
- Find tasks that highlights the strength of DSL
  - storytelling (seemingly sequential), a "main" program (seemingly reactive), a multi-robot task
- Other highlights
  - smart defaults
    - how it handles sequential actions
    - how it handles potentially ambiguous action results
  - possible to make low-level adjustments
  - Record & replay
  - Survey and information gathering
  - Synthesis and verification friendly

---

## 2020-06-04

### Ideas for the Mike & Patricia collaboration

Idea 1: Revisiting the current goals and execution paths. Here are the current goals and plans:

- Editor
  - goals: lightweight, text-based, responsive
  - plan: build something like [mermaid live editor](https://mermaid-js.github.io/mermaid-live-editor/#/edit/)
- State & event visualizer (not a sensor data visualizer)
  - goals: real-time, temporally-summative
  - plan: build something like [rqt-bag](https://images.app.goo.gl/5RUKcqKQwvLVJ3kx6)
- Program controller
  - goals: obedient, responsive, always-on
  - plan: build something like the CodeIt! executor

Idea 2: Revisiting the evaluation ideas. Here are the current evaluation ideas:

1. Series of use case examples; similar to how [vega-lite](https://ieeexplore.ieee.org/document/7539624) evaluates their work
2. Case studies involving the EMAR team; similar to how [rosette paper](https://dl.acm.org/doi/pdf/10.1145/2509578.2509586) evaluates their work

Some thoughts

- Any way to iteratively evaluate the system? e.g., while building it?
- Intuitive-ness study?

---

## 2020-05-18

Desired components

- DSL editor with syntax check
- Record and replay like rosbag or cycle-time-travel
  - real-time state and event visualization

System architecture

- communicate between components via database instead of ROS, websocket, REST API, etc.
- `withRecorder` and `MongoDBDriver` or `FirebaseDriver` for the record and replay component
- language-independent components, e.g., record and replay using cycle-time-travel and node-gui in js but able to communicate with rxpy-cozmo
